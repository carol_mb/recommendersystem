#############################################
# Representation of Anime 
class Anime:
	# key : int
	# name : string
	# year : int
	# ttype : string
	# genres: list of strings
	# numberEps : int
	# ratingMal : float (0 - 10)
	# source : string
	# status : string
	def __init__(self, key, name, year, ttype, genres, numberEps, 
		ratingMAL, source, status):
		self.key = key
		self.name = name
		self.year = year
		self.ttype = ttype
		self.genres = genres
		self.numberEps = numberEps
		self.ratingMAL = ratingMAL
		self.source = source
		self.status = status

	def genresToString(self):
		s = ''
		for g in self.genres:
			s += ' ' + g + ','
		return s[:-1]

	def toString(self):
		tostr = self.key + '; ' + self.name + '; ' + self.year + '; ' + self.ttype + ';' 
		tostr += self.genresToString() + '; ' + self.numberEps + '; ' + self.ratingMAL + '; ' 
		tostr += self.source + '; ' + self.status
		return tostr
