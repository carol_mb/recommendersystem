import requester 
from anime import Anime
from bs4 import BeautifulSoup 

def getGenres(xmlPage):
	xmlPage = xmlPage.lower()
	selectedGenres = []
	genres = urlGenres.keys()
	for g in genres:
		if '>' + g + '<' in xmlPage:
			selectedGenres.append(g)
	return selectedGenres

def getInformation(xmlPage):
	iteration = iter(xmlPage.split('\n'))
	for line in iteration:
		if 'Type:</span>' in line:
			type_ = next(iteration).split('>')[1].split('<')[0]
			continue
		if 'Episodes:</span>' in line:
			numberEps = next(iteration).replace(' ', '')
			continue
		if 'Status:</span>' in line:
			status = next(iteration).replace(' ', '')
			continue
		if 'Aired:</span>' in line:
			year = next(iteration).split()[-1]
			continue
		if 'Source:</span>' in line:
			source = next(iteration).replace(' ', '')
			continue
		if 'Score:</span>' in line:
			ratingMAL = next(iteration).split('>')[1].split('<')[0]
	return type_, numberEps, status, year, source, ratingMAL

def getAnimePageInfo(url):
	xmlPage = requester.requestHTMLPage(url)
	xmlPage = xmlPage.split('<h2>Information</h2>', 1)[1]
	xmlPage = xmlPage.split('(scored by', 1)[0]
	#############################################
	urlSplited = url.split('/')
	key = urlSplited[4]
	name = urlSplited[5]
	#############################################
	genres = getGenres(xmlPage)
	type_, numberEps, status, year, source, ratingMAL = getInformation(xmlPage)
	return Anime(key, name, year, type_, genres, numberEps, 
		ratingMAL, source, status)

# animes : list of anime urls
# write in anime.csv
def preprocessAnimes(animes):
	animesInfo = open(requester.base + 'animes' + '.csv', 'w')
	animesInfo.write("id;name;year;type;genres;episodes;rating;source;status\n") # change string to Anime
	for url in animes:
		current = getAnimePageInfo(url)
		animesInfo.write(current.toString() + '\n')
	animesInfo.close()

# url : from page of a genre
# return list of anime urls from page 
def getAnimesFromHTML(url, maxNumber = 100):
	r = requester.requestHTMLPage(url)
	soup = BeautifulSoup(r, "html.parser")
	links = soup.find_all("a", class_="link-title")
	animes = []
	count = 0
	for link in links:
		animes.append(link.get('href'))
		count += 1
		if count >= maxNumber: 
			break
	return animes

# get list of 100 animes of each genre
def preprocessUrls(urls):
	allAnimes = []
	for genre, url in urls.iteritems():
		current = getAnimesFromHTML(url)
		allAnimes += current
	return allAnimes
		
urlGenres = dict()
urlGenres['mecha'] = 'https://myanimelist.net/anime/genre/18/Mecha' # url and genre name
urlGenres['historical'] = 'https://myanimelist.net/anime/genre/13/Historical'
urlGenres['music'] = 'https://myanimelist.net/anime/genre/19/Music' 

print('Starting preprocessUrls')
animeUrls = preprocessUrls(urlGenres)
print('Starting preprocessAnimes')
preprocessAnimes(animeUrls)
print('Finished all: list of animes in animes.csv')
