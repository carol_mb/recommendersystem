import requester
import pandas as pd

animes = pd.read_csv(requester.base + 'animes.csv', sep=';') # read csv file with animes 
animesID = animes[animes.columns[0]].tolist() # list of all selected animes 
genresID = animes[animes.columns[4]].tolist()
minLen = 5 # min number of animes that a user watched necessary to be seleted

numberClasses = 7
numberUsersByClass = [0] * numberClasses
# class 0: genre 1
# class 1: genre 2
# class 2: genre 3
# class 3: genre 1 and 2
# class 4: genre 2 and 3
# class 5: genre 1 and 3
# class 6: genres 1, 2 and 3
maxUsersByClass = 15
maxUser = maxUsersByClass * numberClasses # number max of selected users


def getGenreID(genre):
	if 'mecha' in genre:
		return 0
	if 'historical' in genre:
		return 1
	if 'music' in genre:
		return 2

def classifyUser(animeDict):
	genres = [0, 0, 0]
	for key in animeDict:
		index = animesID.index(key)
		genre = getGenreID(genresID[index])
		genres[genre] += 1
	for g in genres:
		g = g/len(genres)
	if max(genres) > 0.7:
		return genres.index(max(genres))
	if genres[0] + genres[1] > 0.7:
		return 3
	if genres[1] + genres[2] > 0.7:
		return 4
	if genres[0] + genres[2] > 0.7:
		return 5
	return 6

# anime : json 
# return valid anime id if it is in animesID list
def isValidAnime(anime):
	aID = anime['series_animedb_id']
	if not aID in animesID:
		return -1
	animeScore = anime['my_score']
	if animeScore == 0:
		return -1
	statusWatch = anime['my_status']
	if statusWatch > 4: # watching or completed
		return -1
	return aID

# anime : json
# return str with anime id and user rating 
def getUtilParams(anime):
	return str(anime['series_animedb_id']) + ', ' + str(anime['my_score'])

# js : json (all user animes including animes that won't be used)
# return dict with valid animes or None
def getValidAnimeDict(js):
	listAnimes = js['anime']
	dictValidAnimes = dict()
	for anime in listAnimes:
		validID = isValidAnime(anime) 
		if validID > 0:
			dictValidAnimes[validID] = getUtilParams(anime)
			# print(str(validID) + ' is a valid anime ' + anime['series_title'])
	return dictValidAnimes if len(dictValidAnimes) > minLen else None

def usersByClassValid(dictValidAnimes):
	c = classifyUser(dictValidAnimes)
	numberUsersByClass[c] += 1
	if numberUsersByClass[c] > maxUsersByClass:
		return False
	return True
	
# get user list and select valid users: number min of animes valids and rating all
# return dict key is userID and content is util informations about animes watched
def selectUsers():
	users = open(requester.base + 'users.csv', 'r')
	iteration = iter(users.read().split('\n'))
	usersDict = dict()
	c = 0
	for line in iteration:
		js = requester.requestUserJson(line)
		if not js:
			continue
		userID = js['myinfo']['user_id']
		if not userID in usersDict:
			userAnimeDict = getValidAnimeDict(js)
			if userAnimeDict:
				usersDict[userID] = userAnimeDict
				c = c + 1
				print("anime ok")
				if c > maxUser:
					break
	return usersDict

def dict2csv(usersDict):
	csv = open(requester.base + 'users_items.csv', 'w')
	csv.write('user_id,anime_id,rating\n')
	for key, value in usersDict.iteritems():
		for key2, value2 in value.iteritems():
			csv.write(str(key) + ', ' + value2 + '\n')
	csv.close()

def classifyUsers2csv(usersDict):
	csv = open(requester.base + 'selected_user_class.csv', 'a')
	csv.write('user_id,class\n')
	for key, value in usersDict.iteritems():
		csv.write(str(key) + ', ' + str(classifyUser(value)) + '\n')
	csv.close

users = selectUsers()
#dict2csv(users)
classifyUsers2csv(users)