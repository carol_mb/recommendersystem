import requester
import pandas as pd

maxUsers = 5

def writeUsersFile(xmlPage):
	users = open(requester.base + 'users.csv', 'a')
	iteration = iter(xmlPage.split('\n'))
	count = 0
	for line in iteration:
		if count >= maxUsers:
			break
		if 'https://myanimelist.net/profile' in line:
			username = line.split('</a>')[0].split('<a')[1].split('/')
			if len(username) >= 4: 
				users.write(username[4].split('\"')[0] + '\n')
				count += 1
	users.close()

def getPage(key, name):
	# get list of user watching target anime
	base = 'https://myanimelist.net/anime/'
	url = base + key + '/' + name + '/stats'
	return requester.requestHTMLPage(url)

def preprocessUsers():
	data = pd.read_csv(requester.base + "animes.csv", sep=';')
	for index, row in data.iterrows():
		xmlPage = getPage(str(row[0]), row[1])
		writeUsersFile(xmlPage)

preprocessUsers()