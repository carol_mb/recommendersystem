import time
import requests, base64, sys
from imp import reload
import pandas as pd
from xml.etree.ElementTree import fromstring
from xmljson import parker as bf
import json

base = "../others/"

def getBasicHeader():
	if sys.version[0] == '2':
		reload(sys)
		sys.setdefaultencoding("utf-8")

	usernameAccess = 'carol_mb'
	passwordAccess = 'inteligencia'

	b = base64.b64encode((usernameAccess + ':' + passwordAccess).encode())
	encodedAuth = 'Basic ' + str(b)
	headers = {'Authorization' : encodedAuth}
	return headers

def requestHTMLPage(url):
	h = getBasicHeader()
	r = requests.get(url, headers = h)
	while r.status_code != 200:
		time.sleep(10)
		r = requests.get(url, headers = h)
	return r.content

def getFileToRead(filename):
	file = open(base + filename, 'r')
	fileContent = file.read()
	file.close()
	fileContent = fileContent.split('\n')
	return fileContent

def csv2pd(filename, s = ','):
    return pd.read_csv(base + filename, sep = s) # read csv file with users

def getUserList(user):
	userlist = 'https://myanimelist.net/malappinfo.php?u=' + user + '&status=all&type=anime'
	return requestHTMLPage(userlist)

# username : string 
# return json with user information (list of animes in MAL)
def requestUserJson(username):
	r = getUserList(username)
	return json.loads(json.dumps(bf.data(fromstring(r))))
