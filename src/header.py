import requester
import pandas as pd

minAnimeRated = 20		# min necessary number of animes that a user rated to be seleted
genres = ['mecha', 'historical', 'music']
# read anime information csv
def getAnimespd():
	animespd = []
	for g in genres: 
	    animespd.append(pd.read_csv(requester.base + g + '.csv', sep=';')) # read csv file with animes 
	animespd = pd.concat(animespd)
	return animespd